<%@ page isELIgnored="false" %> <!-- Ensures JSP Expression language is not displayed as plain text -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Simple Servlet/JSP app to demo API call</title>
    <link rel="stylesheet" href="styles/main.css" type="text/css"/>
</head>
<body>
    <h1>OpenWeatherMap API Test</h1>
    <p>To retrieve the temperature for a given location, enter the
       city, state, and country in the field below.</p>
    <p>Examples:<br>
    	<code>London,UK</code><br>
    	<code>Los Angeles,CA,US</code>
    </p>
    <p><i>${message}</i></p>   
    <form action="RapidAPITestServlet" method="post">       
        <label class="pad_top">Location:</label>
        <input type="text" name="location" required><br>        
        <label>&nbsp;</label>
        <input type="submit" value="Submit" class="margin_left">
    </form>
</body>
</html>