package edu.uscb.csci470;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONObject;

import java.net.URLEncoder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
/*
import com.google.gson.JsonObject; // unused import (this code uses org.json.JSONObject, imported above)
*/

/**
 * This is an example of a possible Java servlet that makes a call to get data 
 * from one of the APIs available at RapidAPI.com. This was derived from a 
 * combination of one of the example servlet/JSP-based apps in Murach's 
 * "Java Servlets and JSP" 3rd edition along with selected source code from
 * the RapidAPI "How to use an API with Java" tutorial, available at:
 * https://rapidapi.com/blog/how-to-use-an-api-with-java/
 * 
 * Also: for this example, you'll need to be subscribed to the OpenWeatherMap
 * API at RapidAPI (https://rapidapi.com/community/api/open-weather-map)
 * 
 * @author username@email.uscb.edu
 * @version ICE for 5 March 2021
 */
public class RapidAPITestServlet extends HttpServlet {

	/**
	 * This method specifies what will happen when the submitted form sends 
	 * data to this servlet via a POST request
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
String message = ""; // this is the string that will eventually be forwarded 
		
		/*
		 * Normally, it would be a good idea to validate the data entered on the form
		 * (see the Murach Servlet/JSP slides for Chapter 5 under "a doPost method that 
		 * validates the data"), but that code is omitted in the interest of
		 * keeping this demo example relatively simple. You should always validate 
		 * user-entered data in a production application!! 
		 */
		
		// Set up the URL for the API host and selected character set
		String host = "https://community-open-weather-map.p.rapidapi.com/weather";
		String charset = "UTF-8";
		
		// Headers for a request (here, retrieved from Rapid API website
		String x_rapidapi_host =  "community-open-weather-map.p.rapidapi.com";
		String x_rapidapi_key = "231af3a995msh9796a97b613ea78p1dcb1ajsnbd0ebb190d14";
				
		// Value to include in the API call (see `Unirest.get(...)` below) will come from the specified 
		// field in the submitted form (in this case, see `index.jsp` for the form source code)
		String i = request.getParameter("location");
		
		// Format the query for preventing encoding problems
		String query = String.format("q=%s", URLEncoder.encode(i, charset));
		
		// Json response via selected API (hosted at RapidAPI.com)
		HttpResponse<JsonNode> responseJSON = null; // used `responseJSON` to avoid conflict with `response`
		
		try {
			responseJSON = Unirest.get(host + "?" + query).header("x-rapidapi-host", x_rapidapi_host)
					.header("x-rapidapi-key", x_rapidapi_key).asJson();
		} catch (UnirestException e) {
			// TODO: It is better practice to replace `e.printStackTrace()` with a 
			//       more substantive or meaningful exception handler
			// e.printStackTrace();
			message = "Something went wrong. Try again?";
		} // end try
		
		
		// Prettifying output using GSON (not sure how "pretty" it actually is)
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(responseJSON.getBody().toString());
		String prettyJsonString = gson.toJson(je);
		System.out.println(prettyJsonString); // optional for debugging purposes 
											  // (this will print to standard output, not to HTML)
		
		/*
		 * Code for using Java to parse JSON data was derived from techniques found at:
		 * - https://stackoverflow.com/questions/2591098/how-to-parse-json-in-java
		 * - https://stackoverflow.com/questions/20899839/retrieving-values-from-nested-json-object
		 * 
		 * Remember that Java is case-sensitive; JSONObject (org.json.JSONObject) is NOT
		 * the same class as JsonObject (com.google.gson.JsonObject). Please refer to 
		 * documentation (on your own) to better understand the distinction between the
	     *
		 * (There are plenty of tutorials on the use of Java for parsing JSON files,
		 *  but be sure to take the time to explore the documentation so you know what
		 *  methods to call and what their parameters and return types are.)
		 *  
		 * Note that this simple example does NOT use input validation or exception handling,
		 * so if the user entered a city incorrectly, it will throw an exception.
		 */
		
		// NOTE: "temp" is contained in the "main" object (see the JSON output)
		JSONObject jsonObjectMain = new JSONObject(prettyJsonString).getJSONObject("main");
		String kelvinTemperatureStr = jsonObjectMain.get("temp").toString(); 
		
		// NOTE: the city name can be retrieved directly via the "name" key
		String cityName = new JSONObject(prettyJsonString).getString("name"); 
		
		// convert Kelvin temperature to Fahrenheit and format to use one digit after decimal point
		double kelvinTemperature = Double.parseDouble(kelvinTemperatureStr);
		double celsiusTemperature = kelvinTemperature - 273.15;
		double fahrenheitTemperature = (9.0/5.0)*celsiusTemperature + 32.0;
		String fahrenheitTemperatureStr = String.format("%.1f", fahrenheitTemperature);
		
		// prepare the message for forwarding to another HTML, JSP, or servlet
		// NOTE: If forwarding to a JSP, the attribute name (here, "message")
		// should match up with whatever is used in the JSP Expression Language 
		// tag in the JSP -- e.g., ${message}
		message = "The current temperature in " + cityName + " is " + fahrenheitTemperatureStr + "&deg;F"; 
		request.setAttribute("message", message);
		
		// forward the request to the new (or in this case, reloaded) JSP page
		String url = "/index.jsp";
		getServletContext()
	        .getRequestDispatcher(url)
	        .forward(request, response);
		
	} // end method doPost

} // end class RapidAPITestServlet
